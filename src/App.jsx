import React from "react";
import { GetWallDimensions } from "./components/wall";

function App() {
  return (
    <div className="App">
      {Array(4)
        .fill(0)
        .map((_item, index) => (
          <GetWallDimensions key={index} />
        ))}
    </div>
  );
}

export default App;
