import React, { useState } from "react";
import { Container, Modal } from "./styles";

export function GetWallDimensions() {
  const [wallDimensions, setWallDimensions] = useState({
    height: "",
    width: "",
  });
  const [wallAttachments, setWallAttachments] = useState({
    windows: "",
    doors: "",
  });
  const [error, setError] = useState("");
  const { height, width } = wallDimensions;
  const { windows, doors } = wallAttachments;
  const timeOutAlert = 5000;

  const handleDimensions = ({ target }) => {
    const { name, value } = target;
    error.length && setError("");
    value < 0
      ? null
      : setWallDimensions((prevState) => ({
          ...prevState,
          [name]: Number(value),
        }));
  };

  const handleAttachments = ({ target }) => {
    const { name, value } = target;
    error.length && setError("");
    value < 0
      ? null
      : setWallAttachments((prevState) => ({
          ...prevState,
          [name]: Number(value),
        }));
  };

  const handleMeasures = () => {
    const maxDimension = 50;
    const minDimension = 1;
    const doorHeight = 1.9;
    const minHeightWithDoor = 2.1;
    const totalDimension = height * width;
    const totalDoorDimension = 0.8 * doorHeight * doors;
    const totalWindowDimension = 2.0 * 1.2 * windows;
    const halfDimension = totalDimension / 2;
    const totalAttachments = totalDoorDimension + totalWindowDimension;

    if (totalDimension > maxDimension || totalDimension < minDimension) {
      setError("dimensões");
    }
    if (doors && height >= minHeightWithDoor) {
      setError("tamanho incompativel com porta");
    }
    if (totalAttachments > halfDimension) {
      setError("portas e janelas");
    }
  };

  return (
    <>
      <Container isValid={height && width}>
        <input
          value={height}
          type="number"
          placeholder="informe a altura"
          name="height"
          onChange={handleDimensions}
        />
        <input
          value={width}
          type="number"
          placeholder="informe o comprimento"
          name="width"
          onChange={handleDimensions}
        />
        <input
          className="attachments"
          value={doors}
          type="number"
          placeholder="informe a quantidade de portas"
          name="doors"
          onChange={handleAttachments}
        />
        <input
          className="attachments"
          value={windows}
          type="number"
          placeholder="informe a quantidade de janelas"
          name="windows"
          onChange={handleAttachments}
        />
        <button onClick={handleMeasures}>Salvar</button>
      </Container>
      <Modal error={error.length} onClick={() => setError("")}>
        {error}
      </Modal>
    </>
  );
}
