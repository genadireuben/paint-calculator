import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  .attachments,
  button {
    display: ${({ isValid }) => (isValid ? "block" : "none")};
  }
`;

export const Modal = styled.div`
  width: 80px;
  height: ${({ error }) => (error ? 50 : 0)}px;
  background-color: #f8d7da;
  color: #721c24;
  opacity: ${({ error }) => (error ? 1 : 0)};
  transform: scale(${({ error }) => (error ? 1 : 0)});
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
`;
