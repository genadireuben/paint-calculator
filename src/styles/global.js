import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: 'Roboto', sans-serif;
}

html {
    @media screen and (max-width: 1080px) {
        font-size: 93.75%;
    }
    @media screen and (max-width: 720px) {
        font-size: 87.5%;
    }
}

button {
    cursor: pointer;
}

[disabled]{
    cursor: not-allowed;
    opacity: 0.6;
}
`